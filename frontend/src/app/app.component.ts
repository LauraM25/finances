import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  private theme: string = 'theme';

  @HostBinding('class') get themeClass(): string {
    return this.theme;
  }
}
