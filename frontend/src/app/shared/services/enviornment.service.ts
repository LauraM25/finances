import { Injectable } from '@angular/core';
import { API } from '@app/shared/constants/api';
import { environment } from 'environments/environment';

@Injectable({providedIn: 'root'})
export class EnvironmentService {
  environmentConfig = environment;

  constructor() { }

  getApiUrl(endpoint: API, ...params: any): string {
    let serverUrl: string = '';

    if (this.environmentConfig.server) {
      const protocol: string = this.environmentConfig.server.secure ? 'https' : 'http';
      const port: string = this.environmentConfig.server.port ? `:${this.environmentConfig.server.port}` : '';
      serverUrl = `${protocol}://${this.environmentConfig.server.host}${port}`;
    }

    const url: string = `${serverUrl}/${endpoint}`;
    return url.replace(/%(\d+)/g, (str, index) => params.length ? params[+index - 1] : '');
  }

}