export enum ActionType {
  ADD = 'Add',
  EDIT = 'Edit',
  DELETE = 'Delete',
}
