import { ActionType } from './action-type.enum';
import { DataType } from './data-type.enum';

export interface SavingsExpenses {
  id?: number;
  date?: string;
  amount?: number;
  category?: string;
  description?: string;
  type?: DataType;
  action?: ActionType;
}
