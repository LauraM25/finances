export enum DataType {
    SAVINGS = 'savings',
    EXPENSES = 'expenses',
}
