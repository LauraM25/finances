export enum API {
  EXPENSES = 'expenses/%1',
  SAVINGS = 'savings/%1'
}