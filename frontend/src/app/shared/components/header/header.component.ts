import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentDate: string;
  userName: string;

  constructor() {
    setInterval(() => {
      this.currentDate = moment().format('DD/MM/YYYY, h:mm:ss a');
    }, 1);
  }

  ngOnInit(): void {
    this.userName = 'Mantea Laura';
  }
}
