import { Component, OnInit } from '@angular/core';

import { SettingsFacade } from './store/settings.facade';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  public users;
  public user = { firstName: null, secondName: null };
  public printers;
  public printer = { name: null, trey: null };

  constructor(private settingsFacade: SettingsFacade) {}

  ngOnInit(): void {
    this.settingsFacade.getUsers();
    this.settingsFacade.getUsers$().subscribe((users) => (this.users = users));

    this.settingsFacade.getPrinters();
    this.settingsFacade
      .getPrinters$()
      .subscribe((printers) => (this.printers = printers));
  }

  saveUser() {
    this.settingsFacade.saveUser({ ...this.user, id: this.user.firstName + 1 });
    this.user = { firstName: null, secondName: null };
  }

  savePrinter() {
    this.settingsFacade.savePrinter({
      ...this.printer,
      id: this.printer.name + 1,
    });
    this.printer = { name: null, trey: null };
  }
}
