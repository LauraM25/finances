import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('http://localhost:3000/users');
  }

  getPrinters() {
    return this.http.get('http://localhost:3000/printers');
  }

  saveUser(user) {
    return this.http.post('http://localhost:3000/users', user);
  }

  savePrinter(printer) {
    return this.http.post('http://localhost:3000/printers', printer);
  }
}
