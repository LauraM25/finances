import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SettingsState } from './settings.state';

const featureSelector = createFeatureSelector('settings');

class SettingsStateProps {
  public static readonly users = (state: SettingsState) => state ? state.users : null;
  public static readonly printers = (state: SettingsState) => state ? state.printers : null;
}

export class SettingsStoreProps {
  public static readonly users = createSelector(featureSelector, SettingsStateProps.users);
  public static readonly printers = createSelector(featureSelector, SettingsStateProps.printers);
}
