import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  GetUsers,
  GetUserError,
  GetUsersSuccess, SaveUser,
  SaveUserError,
  SaveUserSuccess,
  SettingsActionTypes,
  GetPrintersSuccess,
  GetPrintersError,
  SavePrinter,
  SavePrinterSuccess,
  SavePrinterError,
  GetPrinters
} from './settings.actions';

import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { SettingsService } from '../settings.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsEffects {
  constructor(
    private actions: Actions,
    private settingsService: SettingsService
  ) { }

  @Effect({ dispatch: true })
  getUsers$ = this.actions
    .pipe(
      ofType(SettingsActionTypes.GET_USERS),
      switchMap((action: GetUsers) => this.settingsService.getUsers()
        .pipe(
          map(settings => new GetUsersSuccess(settings)),
          catchError(error => of(new GetUserError(error)))
        ))
    );

  @Effect({ dispatch: true })
  getPrinters$ = this.actions
    .pipe(
      ofType(SettingsActionTypes.GET_PRINTERS),
      switchMap((action: GetPrinters) => this.settingsService.getPrinters()
        .pipe(
          map(settings => new GetPrintersSuccess(settings)),
          catchError(error => of(new GetPrintersError(error)))
        ))
    );

  @Effect({ dispatch: true })
  saveUser$ = this.actions
    .pipe(
      ofType(SettingsActionTypes.SAVE_USER),
      switchMap((action: SaveUser) => this.settingsService.saveUser(action.user)
        .pipe(
          map(settings => new SaveUserSuccess(settings)),
          catchError(error => of(new SaveUserError(error)))
        ))
    );

  @Effect({ dispatch: true })
  savePrinters$ = this.actions
    .pipe(
      ofType(SettingsActionTypes.SAVE_PRINTER),
      switchMap((action: SavePrinter) => this.settingsService.savePrinter(action.printer)
        .pipe(
          map(settings => new SavePrinterSuccess(settings)),
          catchError(error => of(new SavePrinterError(error)))
        ))
    );
}
