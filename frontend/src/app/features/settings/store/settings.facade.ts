import { Injectable } from '@angular/core';

import { GetPrinters, GetUsers, SavePrinter, SaveUser } from './settings.actions';
import { SettingsState } from './settings.state';
import { SettingsStoreProps } from './settings.store';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class SettingsFacade {
  constructor(private store: Store<SettingsState>) { }

  getUsers() {
    this.store.dispatch(new GetUsers());
  }

  getUsers$() {
    return this.store.select(SettingsStoreProps.users);
  }

  saveUser(user) {
    this.store.dispatch(new SaveUser(user));
  }

  getPrinters() {
    this.store.dispatch(new GetPrinters());
  }

  getPrinters$() {
    return this.store.select(SettingsStoreProps.printers);
  }

  savePrinter(printer) {
    this.store.dispatch(new SavePrinter(printer));
  }
}
