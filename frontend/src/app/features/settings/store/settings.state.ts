export interface SettingsState {
  users: any;
  printers: any;
}

export const initialSettingsState: SettingsState = {
  users: null,
  printers: null
};
