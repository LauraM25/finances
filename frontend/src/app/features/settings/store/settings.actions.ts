import { HttpErrorResponse } from '@angular/common/http';

import { Action } from '@ngrx/store';

export enum SettingsActionTypes {
  GET_USERS = '[Settings] Get users',
  GET_USERS_SUCCESS = '[Settings] Get users success',
  GET_USER_ERROR = '[Settings] Get users error',

  GET_PRINTERS = '[Settings] Get printers',
  GET_PRINTERS_SUCCESS = '[Settings] Get printers success',
  GET_PRINTERS_ERROR = '[Settings] Get printers error',

  SAVE_USER = '[Settings] Save user',
  SAVE_USER_SUCCESS = '[Settings] Save user success',
  SAVE_USER_ERROR = '[Settings] Save user error',

  SAVE_PRINTER = '[Settings] Save printer',
  SAVE_PRINTER_SUCCESS = '[Settings] Save printer success',
  SAVE_PRINTER_ERROR = '[Settings] Save printer error',
}

export class GetUsers implements Action {
  readonly type = SettingsActionTypes.GET_USERS;
  constructor() { }
}

export class GetUsersSuccess implements Action {
  readonly type = SettingsActionTypes.GET_USERS_SUCCESS;
  constructor(public users: any) { }
}

export class GetUserError implements Action {
  readonly type = SettingsActionTypes.GET_USER_ERROR;
  constructor(public error: HttpErrorResponse) { }
}

export class GetPrinters implements Action {
  readonly type = SettingsActionTypes.GET_PRINTERS;
  constructor() { }
}

export class GetPrintersSuccess implements Action {
  readonly type = SettingsActionTypes.GET_PRINTERS_SUCCESS;
  constructor(public printers: any) { }
}

export class GetPrintersError implements Action {
  readonly type = SettingsActionTypes.GET_PRINTERS_ERROR;
  constructor(public error: HttpErrorResponse) { }
}

export class SaveUser implements Action {
  readonly type = SettingsActionTypes.SAVE_USER;
  constructor(public user: any) { }
}

export class SaveUserSuccess implements Action {
  readonly type = SettingsActionTypes.SAVE_USER_SUCCESS;
  constructor(public user: any) { }
}

export class SaveUserError implements Action {
  readonly type = SettingsActionTypes.SAVE_USER_ERROR;
  constructor(public error: HttpErrorResponse) { }
}

export class SavePrinter implements Action {
  readonly type = SettingsActionTypes.SAVE_PRINTER;
  constructor(public printer: any) { }
}

export class SavePrinterSuccess implements Action {
  readonly type = SettingsActionTypes.SAVE_PRINTER_SUCCESS;
  constructor(public printer: any) { }
}

export class SavePrinterError implements Action {
  readonly type = SettingsActionTypes.SAVE_PRINTER_ERROR;
  constructor(public error: HttpErrorResponse) { }
}

export type SettingsActions =
  | GetUsers
  | GetUsersSuccess
  | GetUserError

  | GetPrinters
  | GetPrintersSuccess
  | GetPrintersError

  | SaveUser
  | SaveUserSuccess
  | SaveUserError

  | SavePrinter
  | SavePrinterSuccess
  | SavePrinterError;
