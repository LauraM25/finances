import { SettingsActions, SettingsActionTypes } from './settings.actions';
import { initialSettingsState, SettingsState } from './settings.state';

export const newState = (state, newData) => Object.assign({}, state, newData);

export const settingsReducer = (
  state: SettingsState = initialSettingsState,
  action: SettingsActions
) => {
  switch (action.type) {
    case SettingsActionTypes.GET_USERS_SUCCESS:
      return {
        ...state,
        users: action.users,
      };

    case SettingsActionTypes.GET_PRINTERS_SUCCESS:
      return {
        ...state,
        printers: action.printers,
      };

    case SettingsActionTypes.SAVE_USER_SUCCESS:
      return {
        ...state,
        users: [...state.users, action.user],
      };

    case SettingsActionTypes.SAVE_PRINTER_SUCCESS:
      return {
        ...state,
        printers: [...state.printers, action.printer],
      };
    default:
      return state;
  }
};
