import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';

import { EffectsModule } from '@ngrx/effects';
import { SettingsEffects } from './store/settings.effects';
import { StoreModule } from '@ngrx/store';
import { settingsReducer } from './store/settings.reducer';

import { SettingsComponent } from './settings.component';

import { SettingsRoutingModule } from './settings.routing';

import { SettingsService } from './settings.service';

@NgModule({
  declarations: [
    SettingsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,

    SettingsRoutingModule,
    HttpClientModule,

    StoreModule.forFeature(
      'settings',
      settingsReducer
    ),
    EffectsModule.forFeature([
      SettingsEffects
    ]),
  ],
  providers: [
    SettingsService
  ]
}
)
export class SettingsModule { }
