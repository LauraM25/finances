import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject } from 'rxjs';

import { SavingsExpenses } from '@app/shared/models/savings-expenses.interface';
import { EnvironmentService } from '@app/shared/services/enviornment.service';
import { API } from '@app/shared/constants/api';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  public hasSavingsChanged: boolean;
  public hasExpensesChanged: boolean;
  public expenses$ = new BehaviorSubject<SavingsExpenses[]>([]);
  public savings$ = new BehaviorSubject<SavingsExpenses[]>([]);

  constructor(
    private http: HttpClient,
    private envService: EnvironmentService
    ) { }

  public getExpenses(): any {
    this.http.get(this.envService.getApiUrl(API.EXPENSES)).subscribe((expenses: SavingsExpenses[]) => this.expenses$.next(expenses))
  }

  public getSavings(): any {
    this.http.get(this.envService.getApiUrl(API.SAVINGS)).subscribe((savings: SavingsExpenses[]) => this.savings$.next(savings))
  }

  public addExpense(expense: SavingsExpenses) {
    this.hasExpensesChanged = true;
    this.hasSavingsChanged = false;
    const expenseData = {
      ...expense,
      id: Date.now()
    }

    this.http.post(this.envService.getApiUrl(API.EXPENSES), expenseData).subscribe(() => this.getExpenses())
  }

  public addSaving(saving: SavingsExpenses) {
    this.hasSavingsChanged = true;
    this.hasExpensesChanged = false;
    const savingData = {
      ...saving,
      id: Date.now()
    }

    this.http.post(this.envService.getApiUrl(API.SAVINGS), savingData).subscribe(() => this.getSavings())
  }

  public editExpense(expense: SavingsExpenses) {
    this.http.put(this.envService.getApiUrl(API.EXPENSES, expense.id), expense).subscribe(() => this.getExpenses())
  }

  public editSaving(saving: SavingsExpenses) {
    this.http.put(this.envService.getApiUrl(API.SAVINGS, saving.id), saving).subscribe(() => this.getSavings())
  }

  public deleteExpense(expense: SavingsExpenses) {
    this.http.delete(this.envService.getApiUrl(API.EXPENSES, expense.id)).subscribe(() => this.getExpenses())
  }

  public deleteSaving(saving: SavingsExpenses) {
    this.http.delete(this.envService.getApiUrl(API.SAVINGS, saving.id)).subscribe(() => this.getSavings())
  }
}
