import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

import { DashboardComponent } from '@app/features/dashboard/dashboard.component';
import { DeleteSavingsExpensesModalComponent } from './components/delete-savings-expenses-modal/delete-savings-expenses-modal.component';
import { SavingsExpensesExpansionTableComponent } from './components/savings-expenses-expansion-table/savings-expenses-expansion-table.component';
import { SavingsExpensesModalComponent } from './components/savings-expenses-modal/savings-expenses-modal.component';

import { DashboardRoutingModule } from '@app/features/dashboard/dashboard.routing';

import { DashboardService } from '@app/features/dashboard/dashboard.service';

@NgModule({
  declarations: [
    DashboardComponent,
    SavingsExpensesExpansionTableComponent,
    SavingsExpensesModalComponent,
    DeleteSavingsExpensesModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,

    DashboardRoutingModule,
    HttpClientModule,
  ],
  providers: [DashboardService, MatDatepickerModule],
})
export class DashboardModule {}
