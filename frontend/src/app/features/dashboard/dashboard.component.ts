import { Component, OnDestroy, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { Subscription } from 'rxjs';

import { SavingsExpensesModalComponent } from './components/savings-expenses-modal/savings-expenses-modal.component';

import { DashboardService } from '@app/features/dashboard/dashboard.service';

import { ActionType } from '@app/shared/models/action-type.enum';
import { SavingsExpenses } from '@app/shared/models/savings-expenses.interface';
import { DataType } from '@app/shared/models/data-type.enum';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  public savings$: Subscription;
  public expenses$: Subscription;
  public hasSavingsChanged: boolean;
  public hasExpensesChanged: boolean;
  public expenses = new MatTableDataSource<SavingsExpenses>();
  public savings = new MatTableDataSource<SavingsExpenses>();

  constructor(
    private dialog: MatDialog,
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.dashboardService.getExpenses();
    this.dashboardService.getSavings();

    this.savings$ = this.dashboardService.savings$.subscribe((data) => {
      this.savings.data = data;
      this.hasSavingsChanged = this.dashboardService.hasSavingsChanged;
      this.hasExpensesChanged = this.dashboardService.hasExpensesChanged;
    });

    this.expenses$ = this.dashboardService.expenses$.subscribe((data) => {
      this.expenses.data = data;
      this.hasExpensesChanged = this.dashboardService.hasExpensesChanged;
      this.hasSavingsChanged = this.dashboardService.hasSavingsChanged;
    });
  }

  ngOnDestroy() {
    this.savings$.unsubscribe();
    this.expenses$.unsubscribe();
  }

  public openAddDialog() {
    this.dialog.open(SavingsExpensesModalComponent, {
      width: '400px',
      data: {
        type: DataType.EXPENSES,
        action: ActionType.ADD,
      },
    });
  }
}
