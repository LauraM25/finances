import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavingsExpensesModalComponent } from './savings-expenses-modal.component';

describe('SavingsExpensesModalComponent', () => {
  let component: SavingsExpensesModalComponent;
  let fixture: ComponentFixture<SavingsExpensesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavingsExpensesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavingsExpensesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
