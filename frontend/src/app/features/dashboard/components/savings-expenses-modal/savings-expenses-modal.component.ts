import { Component, Inject, OnInit } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ActionType } from '@app/shared/models/action-type.enum';
import { SavingsExpenses } from '@app/shared/models/savings-expenses.interface';
import { DataType } from '@app/shared/models/data-type.enum';

import { DashboardService } from '@app/features/dashboard/dashboard.service';

@Component({
  selector: 'app-savings-expenses-modal',
  templateUrl: './savings-expenses-modal.component.html',
  styleUrls: ['./savings-expenses-modal.component.scss'],
})
export class SavingsExpensesModalComponent implements OnInit {
  readonly dataType = DataType;
  readonly actionType = ActionType;

  constructor(
    public dialogRef: MatDialogRef<SavingsExpensesModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SavingsExpenses,
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.data = {
      ...this.data,
      date: this.data.date || new Date().toISOString()
    };
  }

  public onNoClick() {
    this.dialogRef.close();
  }
  public checkOption(type: DataType) {
    this.data.type = type;
  }

  public saveInformation(action: ActionType) {
    if (action === ActionType.ADD) {
      if (this.data.type === DataType.SAVINGS) {
        this.dashboardService.addSaving(this.data);
      }
      else {
        this.dashboardService.addExpense(this.data);
      }
    } else {
      if (this.data.type === DataType.SAVINGS) {
        this.dashboardService.editSaving(this.data);
      }
      else {
        this.dashboardService.editExpense(this.data);
      }

    }
  }
}
