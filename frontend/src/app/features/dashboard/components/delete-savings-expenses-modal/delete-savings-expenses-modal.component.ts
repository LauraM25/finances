import { Component, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { DashboardService } from '@app/features/dashboard/dashboard.service';
import { DataType } from '@app/shared/models/data-type.enum';

import { SavingsExpenses } from '@app/shared/models/savings-expenses.interface';

@Component({
  selector: 'app-delete-savings-expenses-modal',
  templateUrl: './delete-savings-expenses-modal.component.html',
  styleUrls: ['./delete-savings-expenses-modal.component.scss']
})
export class DeleteSavingsExpensesModalComponent {
  readonly dataType = DataType;

  constructor(
    public dialogRef: MatDialogRef<DeleteSavingsExpensesModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SavingsExpenses,
    private dashboardService: DashboardService
  ) { }

  public onNoClick() {
    this.dialogRef.close();
  }

  public deleteInformation() {
    if (this.data.type === DataType.SAVINGS) {
      this.dashboardService.deleteSaving(this.data);
    }
    else {
      this.dashboardService.deleteExpense(this.data);
    }
  }
}
