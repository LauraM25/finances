import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteSavingsExpensesModalComponent } from './delete-savings-expenses-modal.component';

describe('DeleteSavingsExpensesModalComponent', () => {
  let component: DeleteSavingsExpensesModalComponent;
  let fixture: ComponentFixture<DeleteSavingsExpensesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteSavingsExpensesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteSavingsExpensesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
