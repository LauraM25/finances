import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { DeleteSavingsExpensesModalComponent } from '@app/features/dashboard/components/delete-savings-expenses-modal/delete-savings-expenses-modal.component';
import { SavingsExpensesModalComponent } from '@app/features/dashboard/components/savings-expenses-modal/savings-expenses-modal.component';

import { ActionType } from '@app/shared/models/action-type.enum';
import { SavingsExpenses } from '@app/shared/models/savings-expenses.interface';

@Component({
  selector: 'app-savings-expenses-expansion-table',
  templateUrl: './savings-expenses-expansion-table.component.html',
  styleUrls: ['./savings-expenses-expansion-table.component.scss'],
})
export class SavingsExpensesExpansionTableComponent implements OnInit, AfterViewInit {
  @Input() results: MatTableDataSource<SavingsExpenses>;
  @Input() title: string;
  @Input() hasExpensesChanged: boolean;
  @Input() hasSavingsChanged: boolean;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public panelOpenState = false;
  public displayedColumns: string[] = [
    'position',
    'date',
    'amount',
    'category',
    'description',
    'action',
  ];
  public dataSource = new MatTableDataSource<SavingsExpenses>();

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource = this.results;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public getTotalCost() {
    return this.dataSource.filteredData
      .map((result) => result?.amount)
      .reduce((acc, value) => acc + value, 0);
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public editInformation(data: SavingsExpenses) {
    this.dialog.open(SavingsExpensesModalComponent, {
      width: '400px',
      data: {
        ...data,
        action: ActionType.EDIT,
      },
    });

  }

  public deleteInformation(data: SavingsExpenses) {
    this.dialog.open(DeleteSavingsExpensesModalComponent, {
      width: '400px',
      data: {
        ...data,
        action: ActionType.DELETE,
      },
    });
  }
}
