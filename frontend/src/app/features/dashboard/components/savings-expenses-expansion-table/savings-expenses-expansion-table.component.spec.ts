import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavingsExpensesExpansionTableComponent } from './savings-expenses-expansion-table.component';

describe('SavingsExpensesExpansionTableComponent', () => {
  let component: SavingsExpensesExpansionTableComponent;
  let fixture: ComponentFixture<SavingsExpensesExpansionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavingsExpensesExpansionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavingsExpensesExpansionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
