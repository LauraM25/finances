import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from '@app/app.component';

import { AppRoutingModule } from '@app/app.routing';
import { DashboardModule } from '@app/features/dashboard/dashboard.module';
import { HeaderModule } from './shared/components/header/header.module';
import { SharedModule } from '@app/shared/shared.module';
import { EnvironmentService } from '@app/shared/services/enviornment.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    DashboardModule,
    HeaderModule,
    RouterModule,
    SharedModule,

    StoreModule.forRoot({}, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictActionWithinNgZone: true
      },
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: true,
    }),
    EffectsModule.forRoot([]),
  ],
  providers: [
    EnvironmentService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
