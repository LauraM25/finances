# Finances

## Open the backend server

1. Navigate to the backend folder
2. Run "npm ci" to install dependencies
3. Run "npm start" to start the mock backend server

## Open the frontend application

1. Navigate to the frontend folder
2. Run "npm ci" to install dependencies
3. Run "npm start"
4. Open http://localhost:4200/

